options(bitmapType = "cairo")

library(DT)
library(lattice)
library(latticeExtra)
library(leaflet)
library(shiny)
library(shinyBS)
library(shinydashboard)
library(shinydashboardPlus)
library(shinyWidgets)
library(tidyr)

source("helpers.r")

ui <- shinydashboardPlus::dashboardPage(
  title = "SSD Temperature induced mortality",
  skin  = "blue-light",
  shinydashboardPlus::dashboardHeader(title = "SSD Temperature induced mortality", titleWidth = "400px"),
  shinydashboardPlus::dashboardSidebar(width = "400px", id = "sidebar",
                                       sidebarMenu(id = "sideMenu",
                                                   menuItem("Navigate", icon = icon("compass"),
                                                            menuSubItem("SSD", tabName = "tabSSD", icon = icon("biohazard")),
                                                            menuSubItem("BoxPlots", tabName = "tabBox", icon = icon("box-open")),
                                                            menuSubItem("Cluster analyses", tabName = "tabCluster", icon = icon("sitemap")),
                                                            menuSubItem("Regression results", tabName = "tabReg", icon = icon("chart-line")),
                                                            menuSubItem("Raw regression data", tabName = "tabRaw", icon = icon("chart-bar")),
                                                            menuSubItem("Field example", tabName = "tabField", icon = icon("thermometer-half")),
                                                            menuSubItem("Information", tabName = "tabInfo", icon = icon("info-circle"), selected = T)
                                                   )
                                       ),
                                       conditionalPanel("input.sidebar",
                                                        fluidPage(
                                                          tags$style(type="text/css", "#blacklabels {color: black}"),
                                                          div(id="blacklabels",
                                                              fluidRow(
                                                                sliderInput("sliderAcclTemp", HTML("<i>T<sub>a</sub></i> (&deg;C)"), 0, 30, 12.5, 0.5),
                                                                bsTooltip("sliderAcclTemp", includeText("www/ta.txt")),
                                                                selectInput("selectKey", "Select key", colnames(species_data), "Subphylum"),
                                                                bsTooltip("selectKey", "Select aspect to be used in plot legends.", placement = "top"),
                                                                sliderInput("sliderRegressConf", "Regression intervals (%)", 1, 99, 95),
                                                                bsTooltip("sliderRegressConf", "Interval (percentage) to be used in regression."),
                                                                selectInput("selectIntervalType", "Interval type", c("confidence", "prediction"), "confidence"),
                                                                bsTooltip("selectIntervalType", "Interval type to be used in regression.", placement = "top")
                                                              ),
                                                              fluidRow(
                                                                column(9, checkboxInput("checkExtrapolate", "Extrapolate from 50% to 10% mortality", F), offset = 0, style = "padding:0px;"),
                                                                bsTooltip("checkExtrapolate", includeText("www/extrapolate.txt")),
                                                                column(3, dropdown(
                                                                  "Data used to extrapolate from 50% effect to a more conservative 10% effect level.",
                                                                  plotOutput("plotExtrapolate"), icon = icon("info-circle"),
                                                                  style = "unite", width="400px", up = T,
                                                                  animate = animateOptions(
                                                                    enter = animations$fading_entrances$fadeInLeftBig,
                                                                    exit = animations$fading_exits$fadeOutRightBig
                                                                  )), offset = 0, style = "padding:0px;")
                                                              ),
                                                              fluidRow(
                                                                checkboxInput("checkUseAll", "Use all data", F),
                                                                bsTooltip("checkUseAll", "Use all data and not only those checked in the raw regression data table.")
                                                              )
                                                          )
                                                        )
                                       )
  ),
  dashboardBody(
    tabItems(
      tabItem(tabName = "tabSSD",
              checkboxInput("checkSSDText", "Show text", T),
              conditionalPanel("input.checkSSDText", includeHTML("www/ssdplot.html")),
              fluidRow(
                column(3, selectInput("selectSSDLabels", "Label markers", c("None", colnames(species_data)), "None")),
                bsTooltip("selectSSDLabels", "Add text labels to the markers in the SSD plot.", placement = "top"),
                column(3, HTML("<label></label>"), checkboxInput("checkSSDregrConf", "Show regression intervals", T)),
                bsTooltip("checkSSDregrConf", "Show the uncertainty in the Temperature Tolerance Interval (TTI) of individual species, resulting from the applied linear regression."),
                column(3, HTML("<label></label>"), checkboxInput("checkSSDConf", "Show SSD confidence", T)),
                bsTooltip("checkSSDConf", "Show the confidence in the S-shaped curve using the method described by Aldenberg et al. (2002) based on the amount of available data."),
                column(3, conditionalPanel("input.checkSSDConf",
                                           sliderInput("sliderSSDConf", "SSD confidence interval (%)", 1, 99, 95, 1))
                ),
                bsTooltip("sliderSSDConf", "Select the level of confidence to show for the S-shaped curve.")
              ),
              fluidRow(
                plotOutput("plotSSD")
              )
      ),
      tabItem(tabName = "tabBox",
              includeText("www/boxplot.txt"),
              checkboxInput("checkSplitOverlap", "Split overlapping groups", T),
              bsTooltip("checkSplitOverlap", "Split species that fall into multiple groups (such as climate zones) into separate groups."),
              plotOutput("plotBox")
      ),
      tabItem(tabName = "tabCluster",
              includeHTML("www/cluster.html"),
              plotOutput("plotCluster")
      ),
      tabItem(tabName = "tabReg",
              includeHTML("www/regresult.html"),
              DTOutput("dtRegressionResult")
      ),
      tabItem(tabName = "tabRaw",
              fluidPage(
                fluidRow(
                  includeHTML("www/raw.html"),
                  selectizeInput("selectSpeciesReg", "Select species to display", width = "100%",
                                 choices = levels(regression_data$Latin), selected = levels(regression_data$Latin)[1],
                                 multiple = T, options = list(plugins = list("remove_button"), create = T, persist = F))
                ),
                fluidRow(
                  column(6, plotOutput("plotRegressionLines")),
                  column(6, DTOutput("dtRegressionData"))
                )
              )
      ),
      tabItem(tabName = "tabField",
              fluidPage(
                fluidRow(
                  column(3, sliderInput("sliderInterpol", "Interpolation steps", 50, 500, 50, 10)),
                  bsTooltip("sliderInterpol", "Upstream and downstream temperatures are not measured at exactly the same time indices. Interpolation is therefore required to compare the two. This slider lets you control the amount of data points that you wish to interpolate."),
                  column(9, includeHTML("www/field.html"))
                ),
                fluidRow(
                  column(6, plotOutput("plotField")),
                  column(6, leafletOutput("mapStations"))
                )
              )
      ),
      tabItem(tabName = "tabInfo",
              fluidPage(
                includeHTML("www/technical.html")
              )
      )
    )
  )
)

server <- function(input, output, session) {
  getRegressionParams <- reactive({
    if (!is.null(input$checkboxClick$i)) regression_data$Selected[input$checkboxClick$i] <<- !regression_data$Selected[input$checkboxClick$i]
    get_regression(input$checkUseAll)
  })
  
  getTableData <- reactive({
    r <- getRegressionParams()
    r <- regression_data
    r$`<i>TTI</i> (&deg;C)` <- round(r$`<i>TTI</i> (&deg;C)`, 1)
    r$`<i>LT50</i> (&deg;C)` <- round(r$`<i>LT50</i> (&deg;C)`, 1)
    r$Latin <- sprintf("<i>%s</i>", r$Latin)
    r$Selected <- sprintf("<input type='checkbox' onclick='Shiny.setInputValue(\"checkboxClick\", {i: %s, t: new Date().getTime()});' %s>", rownames(r), ifelse(r$Selected, "checked", ""))
    r
  })
  
  proxy <- DT::dataTableProxy("dtRegressionData")
  
  observe({
    DT::replaceData(proxy, getTableData(), resetPaging = F, clearSelection = "none")
  })
  
  output$dtRegressionData <- renderDT({
    isolate({ getTableData() })
  }, escape = F)
  
  output$dtRegressionResult <- renderDT({
    r <- getRegressionParams()[[1]]
    r <- as.data.frame(lapply(r, function(x) {
      if (is.numeric(x)) {
        x <- signif(x, 3)
      }
      return (x)
    }), check.names = F)
    r$`Intercept p-value` <- round(r$`Intercept p-value`, 3)
    r$`Intercept p-value`[r$`Intercept p-value` < 0.001] <- "< 0.001"
    r$`Slope p-value` <- round(r$`Slope p-value`, 3)
    r$`Slope p-value`[r$`Slope p-value` < 0.001] <- "< 0.001"
    r
  }, escape = F, rownames = F)
  
  output$plotRegressionLines <- renderPlot({
    rp <- getRegressionParams()
    selected <- rep(T, nrow(regression_data))
    if (!input$checkUseAll) selected <- regression_data$Selected
    print(
      xyplot(`<i>TTI</i> (&deg;C)` ~ `<i>T<sub>a</sub></i> (&deg;C)` | Latin,
             subset(regression_data, Latin %in% req(input$selectSpeciesReg) & selected),
             group = Feature,
             asp = "iso",
             as.table = T,
             xlab = expression(paste("Acclimatisation temperature (", degree, "C)")),
             ylab = expression(paste("Temperature tolerance interval (", degree, "C)")),
             panel = function(x, y, ...) {
               mod <- panel.confidence(x, y, c(-5, 40), level = req(input$sliderRegressConf)/100, interval = input$selectIntervalType)
               panel.grid(h = -1, v = -1)
               if (length(x) > 1) panel.abline(mod)
               panel.xyplot(x, y, ...)
             },
             par.settings = theme,
             scales = list(axs = "i", alternating = F, lim = c(-0.5, 36.5)))
    )
  })
  
  output$plotSSD <- renderPlot({
    ci <- input$sliderSSDConf/100
    if (!input$checkSSDConf) ci <- NULL
    dat <- ssd_fun(input$sliderAcclTemp, input$selectKey, getRegressionParams(), input$checkExtrapolate,
                   input$sliderRegressConf/100, input$selectIntervalType, ci)
    print(
      xyplot(PAF ~ TTI, dat[["data"]], type = "p",
             groups = dat[["data"]][[input$selectKey]],
             xlab = expression(paste("Temperature tolerance interval (", degree, "C)")),
             ylab = "Potentially Affected Fraction of species",
             scales = list(x = list(lim = c(-5, 40))),
             auto.key = list(columns = 2, space = "right"),
             par.settings = theme,
             panel = function(x, y, subscripts, ...) {
               m <- mean(x)
               s <- sd(x)
               panel.grid(h = -1, v = -1)
               panel.xyplot(dat[["curve"]]$HCx, dat[["curve"]]$prob, groups = dat[["curve"]]$ci,
                            type = "l", lty = c(2, 1, 2), subscripts = seq_along(dat[["curve"]]$HCx),
                            col = theme$superpose.line$col[c(3, 2, 3)])
               if (input$checkSSDregrConf) {
                 x0 <- dat[["data"]]$`TTI lwr`[subscripts]
                 x1 <- dat[["data"]]$`TTI upr`[subscripts]
                 panel.arrows(x0, y, x1, y, length = 0.01, angle = 90, code = 3, lty = 2)
               }
               panel.xyplot(x, y, subscripts = subscripts, ...)
               if (input$selectSSDLabels != "None") {
                 lab <- dat[["data"]][[input$selectSSDLabels]][subscripts]
                 lab[is.na(lab)] <- ""
                 panel.pointLabel(x, y, lab, cex = 0.5,
                                  col = adjustcolor(theme$add.text$col, alpha = 0.5))
               }
             })
    )
  })
  
  output$plotCluster <- renderPlot({
    r <- getRegressionParams()[[1]]
    d <- as.matrix(r[,c("Intercept", "Slope")])
    row.names(d) <- r$Latin
    d <- dist(d)
    par(oma = c(0, 0, 0, 0), mai = c(2, 1, 0, 0))
    d <- as.dendrogram(hclust(d))
    labelCol <- function(x) {
      if (is.leaf(x)) {
        label <- attr(x, "label")
        k <- species_data[species_data$Latin == label, input$selectKey][[1]]
        k <- match(k, unique(species_data[[input$selectKey]]))
        n <- length(theme$superpose.line$col)
        attr(x, "nodePar") <- list(lab.col = theme$superpose.line$col[1 + ((k - 1) %% n)])
      }
      return(x)
    }
    
    ## apply labelCol on all nodes of the dendrogram
    d <- dendrapply(d, labelCol)
    
    plot(d)
  })
  
  output$plotField <- renderPlot({
    upstream   <- with(subset(waterbase, Location =="Upstream"), approxfun(Datetime, `Surface temperature`))
    downstream <- with(subset(waterbase, Location =="Downstream"), approxfun(Datetime, `Surface temperature`))
    data <- c(max(c(min(subset(waterbase, Location =="Upstream")$Datetime),
                    min(subset(waterbase, Location =="Downstream")$Datetime))),
              min(c(max(subset(waterbase, Location =="Upstream")$Datetime),
                    max(subset(waterbase, Location =="Downstream")$Datetime))))
    data <- data.frame(Datetime = seq(data[1], data[2], length.out = req(input$sliderInterpol)))
    data$Ta      <- upstream(data$Datetime)
    data$delta_t <- downstream(data$Datetime) - data$Ta
    param <- getRegressionParams()
    data$PAF <- unlist(lapply(seq_along(data$Ta), function(i) {
      tti <- param[[1]]$Intercept + param[[1]]$Slope*data$Ta[i]
      if (input$checkExtrapolate) tti <- extrapolate(tti, data$Ta[i])
      pnorm(data$delta_t[i], mean(tti), sd(tti))
    }))
    ppaf  <- xyplot(PAF ~ Datetime, data,
                    type = "l",
                    xlab = "Date",
                    ylab = "",
                    panel = function(...) {
                      panel.abline(h = seq(0, 1, 0.01), col = theme$reference.line$col, lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
                      panel.abline(h = 0.05, col = "red", lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
                      panel.abline(v = as.POSIXct(sprintf("%i-01-01 00:00", 1988:1993), tz = "CET"),
                                   col = theme$reference.line$col, lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
                      panel.xyplot(...)
                    },
                    par.settings = theme)
    ptemp <- xyplot(`Surface temperature` ~ Datetime, waterbase, groups = Location,
                    scales = list(alternating = F),
                    type = "b",
                    auto.key = list(columns = 2),
                    panel = function(...) {
                      panel.abline(h = 5*(0:5), col = theme$reference.line$col, lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
                      panel.abline(v = as.POSIXct(sprintf("%i-01-01 00:00", 1988:1993), tz = "CET"),
                                   col = theme$reference.line$col, lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
                      panel.xyplot(...)
                    },
                    par.settings = theme)
    pdelta <- xyplot(delta_t ~ Datetime, data,
                     type = "l",
                     panel = function(...) {
                       panel.abline(h = 2*(-1:3), col = theme$reference.line$col, lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
                       panel.abline(v = as.POSIXct(sprintf("%i-01-01 00:00", 1988:1993), tz = "CET"),
                                    col = theme$reference.line$col, lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
                       panel.xyplot(...)
                     })
    print(
      c(`Potentially Affected Fraction` = ppaf,
        `Temperature increase` = pdelta,
        `Surface temperature` = ptemp,
        x.same = T, y.same = F, layout = c(1, 3), merge.legends = T)
    )
  })
  
  output$mapStations <- renderLeaflet({
    leaflet() %>% addTiles() %>%
      setView(lat  = mean(range(stations$lat)), lng = mean(range(stations$long)), zoom = 11) %>%
      addMarkers(lng = stations$long, lat = stations$lat, label = stations$Location) %>% addScaleBar()
  })
  
  output$plotBox <- renderPlot({
    dat <- ssd_fun(input$sliderAcclTemp, input$selectKey, getRegressionParams(), input$checkExtrapolate,
                   input$sliderRegressConf/100, input$selectIntervalType, input$sliderSSDConf/100)[["data"]]
    if (input$checkSplitOverlap) {
      lvls <- if (is.factor(dat[[input$selectKey]])) levels(dat[[input$selectKey]]) else sort(unique(dat[[input$selectKey]]))
      spl <- strsplit(as.character(dat[[input$selectKey]]), "/")
      idx <- inverse.rle(data.frame(lengths = unlist(lapply(spl, length)), values = seq_along(spl)))
      dat <- dat[idx,]
      dat[[input$selectKey]] <- factor(unlist(spl), lvls[lvls %in% unlist(spl)])
    }
    print(
      bwplot(TTI ~ dat[[input$selectKey]], dat, notch = T,
             ylab = expression(paste("Temperature Tolerance Interval (", degree, "C)")),
             prepanel = function(x, y, ...) {
               result <- prepanel.default.bwplot(x, y, ...)
               ## stretch ylim with 10% to make room for text labels
               result$ylim[2] <- result$ylim[2] + .1*diff(result$ylim)
               result
             },
             panel = function(x, y, ...) {
               n <- aggregate(y ~ x, NULL, length)
               panel.abline(h = 2*(-5:24), col = theme$reference.line$col, lty = theme$reference.line$lty, lwd = theme$reference.line$lwd)
               panel.bwplot(x, y, ...)
               panel.text(n$x, max(y) + .07*diff(range(y)), sprintf("N = %i", n$y), cex = 0.9)
             },
             scales = list(x = list(rot = 90)),
             par.settings = theme
      )
    )
  })
  
  output$plotExtrapolate <- renderPlot(plot_extrapolation(level = req(input$sliderRegressConf)/100,
                                                          interval = input$selectIntervalType))
  
  observeEvent(input$linkSSD, updateTabItems(session, "sideMenu", "tabSSD"), ignoreInit = T)
  observeEvent(list(input$linkRegPar, input$linkRegPar2, input$linkRegPar3),
               updateTabItems(session, "sideMenu", "tabReg"), ignoreInit = T)
  observeEvent(list(input$linkRegRaw, input$linkRegRaw2),
               updateTabItems(session, "sideMenu", "tabRaw"), ignoreInit = T)
  
}

shinyApp(ui, server)
