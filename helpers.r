load("data/TSSD.rdata")
load("data/waterbase.rdata")
load("data/extrapolation.rdata")

theme <- custom.theme(symbol = c(RColorBrewer::brewer.pal(9, "Set1"), RColorBrewer::brewer.pal(8, "Set2")),
                      fill = RColorBrewer::brewer.pal(12, "Paired"),
                      region = RColorBrewer::brewer.pal(9, "Blues"),
                      bg = "transparent", fg = "black", pch = 16)
theme$strip.background$col <- "lightgrey"
theme$fontsize$text <- 14
theme$fontsize$points <- 10
theme$box.rectangle$fill <- "white"
theme$box.umbrella$lty <- 1

ks_table <- NULL

extrapolation_mod <- lm(LT10 ~ LT50, extrapolation_data)

plot_extrapolation <- function(level, interval) {
  print(
    xyplot(LT10 ~ LT50, extrapolation_data,
           group = Latin,
           auto.key = list(space = "top", columns = 2, cex = 0.8),
           panel = function(x, y, ...) {
             panel.confidence(x, y, c(-3, 43), level = level, interval = interval)
             panel.grid(-1, -1)
             panel.abline(extrapolation_mod)
             sm <- summary(extrapolation_mod)
             r2 <- round(sm$adj.r.squared, 3)
             panel.text(13, 38, sprintf("N = %i", length(sm$residuals)), cex = 0.8)
             panel.text(13, 35, sprintf("LT10 = %.2f x LT50 %.2f",
                                        sm$coefficients["LT50","Estimate"],
                                        sm$coefficients["(Intercept)","Estimate"]), cex = 0.8)
             panel.text(13, 31, bquote(r[adj]^2==.(r2)), cex = 0.8)
             panel.xyplot(x, y, ...)
           },
           asp = "i",
           xlab = expression(paste("10% lethal temperature (", degree, "C)")),
           ylab = expression(paste("50% lethal temperature (", degree, "C)")),
           par.settings = theme)
  )
}

ssd_fun <- function(Ta, key, rp, extrapolate, level, interval, ssd_level) {
  dat <- do.call(rbind, lapply(names(rp[[2]]), function(species) {
    mod <- rp[[2]][[species]]
    if (is.null(mod[[1]])) return(NULL)
    result <- predict(mod, data.frame(`<i>T<sub>a</sub></i> (&deg;C)` = Ta, check.names = F),
                      type = "response", interval = interval, level = level)
    result <- as.list(result)
    names(result) <- c("TTI", "TTI lwr", "TTI upr")
    result[["Latin"]] <- species
    as.data.frame(result, check.names = F)
  }))
  if (extrapolate) {
    dat$TTI <- extrapolate(dat$TTI, Ta)
    ## TODO include uncertainty in extrapolation?
    dat$`TTI lwr` <- extrapolate(dat$`TTI lwr`, Ta)
    dat$`TTI upr` <- extrapolate(dat$`TTI upr`, Ta)
  }
  dat <- merge(dat, species_data, by = "Latin", all.x = T, all.y = F)
  dat <- dat[order(dat$TTI),]
  lvls <- if (is.factor(dat[[key]])) levels(dat[[key]]) else sort(unique(dat[[key]]))
  dat$PAF <- (seq_along(dat$TTI) - 0.5)/length(dat$TTI)
  dat[[key]] <- as.character(dat[[key]])
  anyna <- if (any(is.na(dat[[key]]))) "Not available" else NULL
  dat[[key]][is.na(dat[[key]])] <- "Not available"
  dat[[key]] <- factor(dat[[key]], c(lvls, anyna))
  if (is.null(ssd_level)) {
    crv <- data.frame(prob = seq(0.01, 0.99, length.out = 30), ci = 0.5)
    crv <- cbind(crv, HCx = qnorm(crv$prob, mean(dat$TTI), sd(dat$TTI)))
    crv <- rbind(crv, data.frame(prob = NA, HCx = NA, ci = c(0, 1)))
  } else {
    if (is.null(ks_table) || ks_table$n[[1]] != nrow(dat) || diff(range(ks_table$ci)) != ssd_level) {
      ks_table <<- updateKsTable(ssd_level, nrow(dat))
    }
    crv <- ks_table
    crv$HCx <- mean(dat$TTI) - ks_table$Ks*sd(dat$TTI)
  }

  list(data = dat, curve = crv)
}

get_regression <- function(use_all = F) {
  dat <- regression_data
  if (!use_all)  dat <- subset(dat, Selected)
  mods <- lapply(unique(dat$Latin), function (species) {
    tryCatch(lm(`<i>TTI</i> (&deg;C)` ~ `<i>T<sub>a</sub></i> (&deg;C)`, subset(dat, Latin == species)),
             error = function(e) {list(NULL)})
  })
  names(mods) <- unique(dat$Latin)
  result <- do.call(rbind, by(dat, dat$Latin, function(x) {
    if (nrow(x) < 2) return(NULL)
    mod     <- mods[[as.character(x$Latin[[1]])]]
    sm     <- summary(mod)
    coefs  <- as.data.frame(t(as.table(sm$coefficients)))
    result <- cbind(data.frame(Latin = x$Latin[[1]]),
                    as.data.frame(pivot_wider(coefs, names_from = c(Var1, Var2), values_from = Freq)))
    names(result)[-1] <- c("Intercept", "Intercept Std. Err.", "Intercept t-value", "Intercept p-value",
                           "Slope", "Slope Std. Err.", "Slope t-value", "Slope p-value")
    result[["R<sup>2</sup><sub>adj</sub>"]] <- sm$adj.r.squared
    result[["N"]] <- nrow(x)
    return(result)
  }))
  rownames(result) <- NULL
  list(result, mods)
}

## helper functions to calculate HCx according to Aldenberg et al.
zero <- function(ks, n, conf, kp)
{
  x <- ks*sqrt(n)
  const <- kp*sqrt(n)
  df <- n - 1
  pt(x, df, const) - conf
}

getKs <- function(n, conf, FA)
{
  uniroot(function(ks) { zero(ks, n, conf, -qnorm(FA, 0, 1)) },
          interval = c(-300, 300))$root
}

getConf <- function(ks, n, FA)
{
  1 - uniroot(function(conf) { zero(ks, n, conf, -qnorm(FA, 0, 1)) },
              interval=c(0, 1))$root
}

updateKsTable <- function(confidence, n) {
  prob_range <- seq(0.01, 0.99, length.out = 30)
  do.call(rbind, lapply(n, function(ni) {
    do.call(rbind, lapply(c(0.5 - confidence/2, 0.5, 0.5 + confidence/2), function(conf) {
      do.call(rbind, lapply(prob_range, function(prob) {
        result <- tryCatch(data.frame(n = ni, ci = conf, Ks = getKs(ni, conf, prob), prob = prob), error = function() {NULL})
        return(result)
      }))
    }))
  }))
}

extrapolate <- function(x, Ta) {
  slp <- extrapolation_mod$coefficients[["LT50"]]
  ntr <- extrapolation_mod$coefficients[["(Intercept)"]]
  return ((slp - 1)*Ta + slp*x + ntr)
}

panel.confidence <- function(x, y, range, level = 0.95, interval = "confidence", col = "lightgrey", length.out = 50) {
  if (length(x) > 1) {
    mod <- lm(y ~ x, NULL)
    pred <- data.frame(x = seq(range[1], range[2], length.out = length.out))
    pred <- cbind(pred, as.data.frame(predict(mod, pred, se.fit = T,
                                              level = level,
                                              interval = interval)[c("fit", "se.fit")]))
    panel.polygon(x = c(pred$x, rev(pred$x), pred$x[1]),
                  y = c(pred$fit.upr, rev(pred$fit.lwr), pred$fit.upr[1]),
                  border = NA, col = col)
    return(mod)
  }
}
